<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Document</title>
	</head>
	<style>
		h1 {
			color: #00FFF0;
		}
	</style>
	<body>
		<center>
			<h1>Correo Electrónico</h1>
			<p>Este es el primer correo que mandaré por Laravel</p>
			<p> <strong>Nombre:</strong> {{$contacto['name']}}</p>
			<p> <strong>Correo:</strong> {{$contacto['correo']}}</p>
			<p> <strong>Mensaje:</strong> {{$contacto['mensaje']}}</p>
		</center>	
		<img alt="Inspect with Tabs" src="https://assets-examples.mailtrap.io/integration-examples/welcome.png" style="width: 100%;">
	</body>
</html>