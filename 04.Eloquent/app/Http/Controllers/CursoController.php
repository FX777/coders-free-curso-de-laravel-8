<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CursoController extends Controller{

    public function index(){
        return view('cursos.index');
    }

    public function create(){
        return view('cursos.create');
    }

    public function store(Request $request)
    {
        //
    }

    public function show($curso, $categoria = ''){
        //['curso' => $curso] === compact('curso');
        return view('cursos.show', compact('curso', 'categoria'));
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
