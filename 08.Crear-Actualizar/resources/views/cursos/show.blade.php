@extends('layouts.plantilla')

@section('title', 'Cursos ' . $curso->name)

@section('content')
	<h2>Bienvenidos al Curso de: {{$curso->name}} </h2>
	<a href="{{ route('cursos.index') }}">Volver a Cursos</a>
	<br>
	<a href="{{ route('cursos.edit', $curso) }}">Editar Curso</a>

	<p><strong>Categoria: </strong>{{$curso->categoria}}</p>
	<p>{{$curso->descripcion}}</p>
@endsection