<?php

namespace Database\Seeders;

use App\Models\Curso;
use Illuminate\Database\Seeder;

class CursoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $curso = new Curso();

    	$curso->name = "Laravel";
    	$curso->descripcion = "El Mejor Framework de PHP";
    	$curso->categoria = "Desarrollo Web";

    	$curso->save();

    	$curso2 = new Curso();

    	$curso2->name = "VueJS";
    	$curso2->descripcion = "El Mejor Framework de Diseño";
    	$curso2->categoria = "Diseño Web";

    	$curso2->save();
    }
}
