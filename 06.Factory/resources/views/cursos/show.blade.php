@extends('layouts.plantilla')

@section('title', 'Cursos ' . $curso)

@section('content')
    
    <h1>Bienvenidos al curso de: {{$curso}} - {{$categoria ?? ''}} </h1>
    
@endsection