<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreCurso extends FormRequest{

	public function authorize(){
		return true;
	}

	public function rules(){

		return [
			'name' => 'required|max:10',
			'descripcion' => 'required|min:10',
			'categoria' => 'required'
		];

	}

	public function attributes(){
		return [
			'name' => 'nombre del curso',
		];
	}

	public function messages(){
		return [
			'descripcion.required' => 'Debe ingresar una descripcion del curso.',
		];
	}
}
