<x-app-layout>
    <x-slot name="header">
        <h2 class="font-bold text-xl text-blue-800 leading-tight">
            Coders Free
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg p-4">
                <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Ipsum excepturi, voluptates laborum nobis vel placeat deserunt eos. Mollitia ea inventore rerum iusto et, accusamus corrupti, magnam fugiat, dolorum itaque molestias.</p>
                <p>Delectus veniam mollitia repellendus minima repellat ducimus error saepe velit illum recusandae, accusamus blanditiis placeat ab? Nam, dolorem. Omnis quibusdam quae nisi dicta dolores, a at ex inventore? Sed, iusto.</p>
                <p>Officia quis incidunt inventore suscipit! Quae, cupiditate eaque eum est minima perferendis accusamus magni reprehenderit praesentium quibusdam, distinctio adipisci consectetur eos maxime perspiciatis repellendus. Autem voluptatibus voluptatum quae esse cumque.</p>
                <p>Ab odit quia inventore quisquam quam quos, repudiandae voluptatibus delectus, eaque impedit fuga obcaecati est totam labore error facere quaerat! Officia eos ipsa ducimus. Sit incidunt doloremque odit tenetur illum.</p>
                <p>Accusamus minima, perferendis adipisci illum aspernatur debitis ullam distinctio laudantium possimus ad neque, nisi aperiam voluptas numquam explicabo quisquam harum consequuntur ratione atque cupiditate recusandae repellat tenetur? Temporibus, tempora quae.</p>
            </div>
        </div>
    </div>
</x-app-layout>
