<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <!-- Styles -->
        <link href="{{asset('css/app.css')}}" rel="stylesheet">

    </head>

    @php
        $color = 'red';
        $alert = 'alert2';
    @endphp

    <body>
        <div class="container mx-auto">
            <x-alert :color="$color" class="mb-4">

                <x-slot name="title">
                    Titulo 01
                </x-slot>

                Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatibus veniam iusto sequi sunt illo eaque modi non officia necessitatibus, veritatis numquam dolore iste. Architecto iure aut nemo reiciendis quibusdam nobis!
            </x-alert>

            <x-alert  class="mb-4">

                <x-slot name="title">
                    Titulo 02
                </x-slot>

                Hola mundo.!
            </x-alert>
            
            <x-alert2 color="blue">
                <x-slot name="title">
                    Titulo de prueba
                </x-slot>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Et modi quasi, reiciendis voluptates officia rem architecto numquam quis dicta sed. Nulla minus rerum corporis pariatur fugiat at ipsum distinctio aspernatur?
            </x-alert2>

            <x-dynamic-component :component="$alert">
                <x-slot name="title">
                    Titulo de prueba
                </x-slot>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Et modi quasi, reiciendis voluptates officia rem architecto numquam quis dicta sed. Nulla minus rerum corporis pariatur fugiat at ipsum distinctio aspernatur?
            </x-dynamic-component>

        </div>
    </body>
</html>
